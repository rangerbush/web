angular.module('myApp')
.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('login');

    $stateProvider
        .state('main', {
            url: '/main',
            templateUrl: '/dashboard/dashboardContainer.html'
        })
        .state('login', {
            url: '/login',
            templateUrl: '/login/login.html',
            controller: 'loginController'
        })
        .state('main.dashboard', {
            url: '/main',
            templateUrl: '/dashboard/dashboard.html',
            controller: 'dashboardCtrl'
        })
        .state('main.account', {
            url: '/account',
            templateUrl: '/dashboard/account/accountContainer.html',
            controller: 'accountCtrl'
        })


    ;

});