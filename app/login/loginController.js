'use strict';
import Swal from '../lib/sweetalert2/src/sweetalert2.js';
 angular.module('myApp')
.controller('loginController', ['$scope', '$http', 'config', '$window',
    function ($scope, $http, config, $window) {

        $scope.email;
        $scope.password;

        $scope.login = function () {
            if ($scope.email === undefined || $scope.email === null) {
                Swal.fire({  icon: 'error',
                    title: 'Oops...',
                    text: 'Please enter your email'});
                return;
            }
            if ($scope.password === undefined || $scope.password === null) {
                // swal("Oops!","Please enter your password.","error");
                Swal.fire({  icon: 'error',
                    title: 'Oops...',
                    text: 'Please enter your password'});
                return;
            }
            $http({
                method: 'GET',
                url: config.apiUrl + 'account/login',
                params: {
                    email: $scope.email,
                    password: $scope.password
                }
            }).then(function successCallback(res) {
                console.log(res);
                if (res.data.status_code == 200) {
                    console.log(res.data.payload[0]);
                    $window.sessionStorage.setItem('user', angular.toJson(res.data.payload[0]));
                    Swal.fire({
                        icon:'success',
                        timer: 2000,
                        timerProgressBar: false,
                        text: 'Log in successfully,\n redirecting to dashboard.',
                        onClose: () => {
                            $window.location.href = '#!/main';
                        }
                    });


                } else  {
                    console.log('status code',res.data.status_code);
                    console.log('message',res.data.message);
                    Swal.fire({  icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong, \nplease check your credentials and try again.'});
                }

            }, function errorCallback(res) {
                console.log(res)
                Swal.fire({  icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong, \nplease check your credentials and try again.'});
            });


        };

    }]);