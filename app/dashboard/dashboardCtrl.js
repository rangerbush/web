'use strict';

angular.module('myApp')
    .controller('dashboardCtrl', ['$scope', '$http', 'config', '$window',
        function ($scope, $http, config, $window) {
            init();
            function init() {
                console.log(' dashboard');
                let u = $window.sessionStorage.getItem('user');
                if (u === undefined || u === null)
                    $window.location.href = '#!/login';  //redirect to login if user info does not exist
                $scope.user = angular.fromJson(u);
                console.log($scope.user);
                $scope.displayedName = $scope.user.email;
                if ($scope.user.lastName !== undefined && $scope.user.lastName !== null)
                    $scope.displayedName = $scope.user.lastName;
                if ($scope.user.firstName !== undefined && $scope.user.firstName !== null)
                    $scope.displayedName = $scope.user.firstName;
            }
        }]);